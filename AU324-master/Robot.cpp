#include <string>
#include <sstream>
#include "Robot.h"
#include "serialib.h"
#include <stdio.h>
#include <stdlib.h>

using namespace std;

Robot::Robot(char* stringPort)
{
    port.Open(stringPort, SERIAL_BAUDRATE);
    char c;
    while (port.Read(&c, 1) <= 0) {
        sleep(1);
    }
}

void Robot::sendOrder(float leftMotor, float rightMotor)
{
    int left = (int)(leftMotor);
    int right = (int)(rightMotor);

    ostringstream oss;
    oss << left << " " << right << ";";

    char result = port.WriteString(oss.str().c_str());
    //port.flush();
    
    //printf("%d", result);
}

