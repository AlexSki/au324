#ifndef _ROBOT_H
#define _ROBOT_H

#include "serialib.h"

#define SERIAL_PORT     "/dev/ttyUSB1"
#define SERIAL_BAUDRATE 115200

class Robot
{
    public:
        Robot(char* stringPort);

        void sendOrder(float leftMotor, float rightMotor);

    protected:
        serialib port;
};

#endif
