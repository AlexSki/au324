int speedl = 0;
int speedr = 0;
int dirl = HIGH;
int dirr = HIGH;

void setup() {
  // Vitesse moteur gauche
  pinMode(9, OUTPUT);
  //Direction moteur gauche
  pinMode(7, OUTPUT);

  // Vitesse moteur droit
  pinMode(10, OUTPUT);
  // Direction moteur gauche
  pinMode(8, OUTPUT);

  Serial.begin(9600);
  Serial.println("c");
}

void loop() {
  int tmpSpeedl;
  int tmpSpeedr;
  
  // Serial  
  if (Serial.available() > 0) {
    //tmpSpeedl = Serial.parseInt();
    //tmpSpeedr = Serial.parseInt();
    tmpSpeedl = Serial.readStringUntil(' ').toInt();
    tmpSpeedr = Serial.readStringUntil(' ').toInt();
    //Serial.print(tmpSpeedl,DEC);
    //Serial.print(" ");
    //Serial.println(tmpSpeedr,DEC);

     if (tmpSpeedl > 0){
        dirl = HIGH;
      } else {
        dirl = LOW;
      }
      if (tmpSpeedr > 0){
        dirr = HIGH;
      } else {
        dirr = LOW;
      }

      // Changer les vitesses
      speedl = constrain(abs(tmpSpeedl), 0, 255);
      speedr = constrain(abs(tmpSpeedr), 0, 255);
   
  }


  
  // Ecriture des sens de rotation
  digitalWrite(7, dirl);
  digitalWrite(8, dirr);

  // Ecriture des vitesses de rotation
  analogWrite(9, speedl);
  analogWrite(10, speedr);
  

  // Affiche les vitesses des roues
  //Serial.print(speedl); // gauche
  //Serial.print(" ");
  //Serial.println(speedr); // droite
}
