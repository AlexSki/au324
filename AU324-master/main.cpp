#include "Robot.h"
#include <iostream>
#include <opencv2/opencv.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "serialib.h"

#define BUF_SIZE 9

using namespace std;
using namespace cv;

/**
 * Fonction de dÃ©monstration
 */
/*void demoRobot() {
  Robot robot;
  while (true) {
    cout << "Ne bouge pas" << endl;
    robot.sendOrder(0, 0);
    getchar();
    cout << "Roues gauche" << endl;
    robot.sendOrder(0.1, 0);
    getchar();
    cout << "Roues droites" << endl;
    robot.sendOrder(0, 0.1);
    getchar();
    cout << "Roues gauche Ã  contre-sens des droites" << endl;
    robot.sendOrder(-0.1, 0.1);
    getchar();
    cout << "Roues gauche et droite Ã  mÃªme allure" << endl;
    robot.sendOrder(0.1, 0.1);
    getchar();
  }
}*/

void demoCV() {
  VideoCapture cap(0);

  cap.set(CV_CAP_PROP_FPS, 15); // Frame rate
  // cap.set(CAP_PROP_FRAME_WIDTH, );// Width of the frames in the video stream.
  // cap.set(CAP_PROP_FRAME_HEIGHT,);// Height of the frames in the video
  // stream.

  if (!cap.isOpened()) {
    cout << "Unable to open the device" << endl;
    return;
  }

  /* while(true)
   {
   Mat frame;
   cap >> frame;
   i*/

    Mat edges;
    for (;;) {
    const clock_t begin_time = clock();
// do something

    Mat frame;
    cap >> frame; // get a new frame from camera
    //cvtColor(frame, edges, COLOR_BGR2GRAY);
    //GaussianBlur(edges, edges, Size(7, 7), 1.5, 1.5);
    //Canny(edges, edges, 0, 30, 3);
    //imshow("edges", edges);

    cvtColor(frame, edges, COLOR_BGR2HSV);

    // GaussianBlur(edges, edges, Size(7,7), 1.5, 1.5);

    // cv::Mat lower_red_hue_range;
    cv::Mat upper_red_hue_range;
    // cv::inRange(hsv_image, cv::Scalar(0, 100, 100), cv::Scalar(10, 255, 255),
    // lower_red_hue_range);
    cv::inRange(edges, cv::Scalar(25, 100, 100), cv::Scalar(35, 255, 255),
                upper_red_hue_range);

    if (waitKey(30) >= 0)
      break;

    std::cout << float( clock () - begin_time ) /  CLOCKS_PER_SEC << std::endl;
  }
  // the camera will be deinitialized automatically in VideoCapture destructor
}

FILE* openSerialComm(char* nameFlow){
    FILE * fd = fopen(nameFlow, "w+");
    //rewind(fd);
    if(fd == NULL)
    {
        exit(EXIT_FAILURE);
    } else {
        printf("Ouverture Serial Comm\n");
    }
    return fd;
}


int sendMessage(int fd, const char* leftMotor, const char* rightMotor){
    
    char buffer[BUF_SIZE];
    
    sprintf(buffer, "%s %s \r\n", leftMotor, rightMotor);
    
    printf("%s\n", buffer);
    //int written = fprintf(fd, buffer);
    write(fd, buffer, 9);
    /*if(written < 0){
        return 0;
    } else {
        printf("Envoi Message %d\n", written);
    }*/
    sleep(5);
    return 1;
}

int readMessage(int fd){
    /*char* buf ;
    buf = (char*) malloc(BUF_SIZE);
    if(fgets(buf,BUF_SIZE,fd)!=NULL){
        printf("coucou %s\n", buf);
        return 1;
    }else{
        std::cout << buf << std::endl;
        printf("Oops %d\n", buf);
        //printf("Oops %c\n", buf);
    }*/
    char* bytes = (char*) malloc(9);
    ssize_t size = read(fd, bytes, 9);
    printf("Read byte %s\n", bytes);
    return 0;
}

int main(int argc, char **argv) {
  // demoRobot();
    //FILE* fd = openSerialComm("/dev/ttyUSB0");
    //int fd = open("/dev/ttyUSB0", O_RDWR);
    
    printf("Send Message Before\n");
    Robot* robot = new Robot(argv[1]);
    //printf("slip\n");
    int i = -60;
    int j = 100;
    for(;;){
        robot->sendOrder(j, i);
        sleep(atoi(argv[2]));
        robot->sendOrder(i, j);
        sleep(atoi(argv[2]));
       //sendMessage(fd, "50", "-50");
       //readMessage(fd);
        if(argv[3] != NULL){
           robot->sendOrder(0, 0); 
           //die;
        }
    }
    printf("Read Message Before\n");
    
    return 0;
}
